import csv
import unittest
from no_of_times_each_city_has_hosted_olympics import no_of_times_each_city_has_hosted_olympics
from ratio_m_fem_by_decade import ratio_m_fem_by_decade
from countries_with_medal_after_2000 import countries_with_medal_after_2000
from avg_age_heavy_weight_boxing_per_year import avg_age_heavy_weight_boxing_per_year
from medals_won_by_india_per_year import medals_won_by_india_per_year
from extract_csv_data import extract_csv_data

athlete1 = extract_csv_data('olympic/mock_athlete_events.csv')

def extracting_data(file):
    reader = csv.DictReader(open(file))
    result={}
    for row in reader:
        for column,value in row.items():
            result.setdefault(column,[]).append(value)

    return result
athlete=extracting_data('olympic/mock_athlete_events.csv')

class Test_olympic(unittest.TestCase):
    def test_no_of_times_each_city_has_hosted_olympics(self):
        expected_output = {'Barcelona': 1, 'Calgary': 1, 'Albertville': 1, 'Lillehammer': 1, 'Stockholm': 1, 'Paris': 1, 'London': 1, 'Nagano': 1, 'Salt Lake City': 1, 'Squaw Valley': 1, 'Rio de Janeiro': 1}
        output = no_of_times_each_city_has_hosted_olympics(athlete)
        self.assertEqual(output,expected_output)

    def test_ratio_m_fem_by_decade(self):
        expected_output = {1990: {'M': 6, 'F': 1}, 1980: {'F': 2}, 1910: {'M': 1}, 1920: {'M': 1}, 1940: {'M': 3}, 2000: {'M': 1}, 1960: {'M': 1}, 2010: {'F': 1}}
        output = ratio_m_fem_by_decade(athlete1)
        self.assertEqual(output,expected_output)

    def test_countries_with_medal_after_2000(self):
        expected_output = {'Silver': {'IND': 1}}
        output = countries_with_medal_after_2000(athlete)
        self.assertEqual(output,expected_output)

    def test_avg_age_heavy_weight_boxing_per_year(self):
        expected_output = {'1948':24}
        output = avg_age_heavy_weight_boxing_per_year(athlete1)
        self.assertEqual(output,expected_output)

    def test_medals_won_by_india_per_year(self):
        expected_output = {'2016': {'Pusarla Venkata "P. V." Sindhu': 'Silver'}}
        output = medals_won_by_india_per_year(athlete1)
        self.assertEqual(output,expected_output)

if __name__ == '__main__':
    unittest.main()