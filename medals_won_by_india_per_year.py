import matplotlib.pyplot as plt
from extract_csv_data import extract_csv_data
import csv
import collections

def medals_won_by_india_per_year(athletes):
    india_medals_per_year = {}
    for athlete in athletes:
        if athlete['NOC'] == 'IND' and athlete['Medal'] != 'NA':
            if athlete['Year'] in india_medals_per_year:
                if athlete['Name'] in india_medals_per_year[athlete['Year']]:
                    india_medals_per_year[athlete['Year']][athlete['Name']]=athlete['Medal']
                else:
                    india_medals_per_year[athlete['Year']][athlete['Name']]=athlete['Medal']
            else:
                india_medals_per_year[athlete['Year']]={athlete['Name']:athlete['Medal']}
    sorted_india_medals_year_wise = collections.OrderedDict(sorted(india_medals_per_year.items()))
    print(sorted_india_medals_year_wise)
    return sorted_india_medals_year_wise

def plot_and_calculate_medals_won_by_india_per_year(athletes):
    sorted_india_medals_year_wise = medals_won_by_india_per_year(athletes)

if __name__ == "__main__":
    athletes = extract_csv_data('olympic/athlete_events.csv')
    medals_won_by_india_per_year(athletes)