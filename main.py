import csv
import matplotlib.pyplot as plt
from no_of_times_each_city_has_hosted_olympics import plot_and_calculate_no_of_times_each_city_has_hosted_olympics
from ratio_m_fem_by_decade import plot_and_calculate_ratio_m_fem_by_decade
from countries_with_medal_after_2000 import plot_and_calculate_countries_with_medal_after_2000
from avg_age_heavy_weight_boxing_per_year import plot_and_calculate_avg_age_heavy_weight_boxing_per_year
from medals_won_by_india_per_year import plot_and_calculate_medals_won_by_india_per_year
from extract_csv_data import extract_csv_data

def extracting_data(file):
    reader = csv.DictReader(open(file))
    result={}
    for row in reader:
        for column,value in row.items():
            result.setdefault(column,[]).append(value)
    return result


def main(file):
    athlete1 = extract_csv_data(file)
    athlete=extracting_data(file)
    plot_and_calculate_no_of_times_each_city_has_hosted_olympics(athlete)
    plot_and_calculate_ratio_m_fem_by_decade(athlete1)
    plot_and_calculate_countries_with_medal_after_2000(athlete)
    plot_and_calculate_avg_age_heavy_weight_boxing_per_year(athlete1)
    plot_and_calculate_medals_won_by_india_per_year(athlete1)

if __name__ == "__main__":
    main('olympic/athlete_events.csv')