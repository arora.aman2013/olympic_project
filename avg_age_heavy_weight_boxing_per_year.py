import matplotlib.pyplot as plt
import csv
from extract_csv_data import extract_csv_data
import collections

def avg_age_heavy_weight_boxing_per_year(athlete):
    sum_of_boxers_age_per_year = {}
    count_boxers_per_year={}
    avg_mens_boxing_age_per_year={}
    for row in athlete:
        if row['Weight']!='NA' and row['Age'] != 'NA':
            if row['Sport'] ==  'Boxing' and float(row['Weight'])>90:
                if row['Year'] in sum_of_boxers_age_per_year:
                    sum_of_boxers_age_per_year[row['Year']]+=int(row['Age'])
                    count_boxers_per_year[row['Year']]+=1
                else:
                    sum_of_boxers_age_per_year[row['Year']]=int(row['Age'])
                    count_boxers_per_year[row['Year']]=1
        for year in sum_of_boxers_age_per_year:
            avg_mens_boxing_age_per_year[year] = sum_of_boxers_age_per_year[year]/count_boxers_per_year[year]
        sorted_avg_age_heavy_weight_boxing_per_year = collections.OrderedDict(sorted(avg_mens_boxing_age_per_year.items()))
    print(sorted_avg_age_heavy_weight_boxing_per_year)
    return sorted_avg_age_heavy_weight_boxing_per_year

def plot_avg_age_heavy_weight_boxing_per_year(avg_mens_boxing_age_per_year):
    years = list(avg_mens_boxing_age_per_year.keys())
    average_age = []
    for year in avg_mens_boxing_age_per_year:
        average_age.append(avg_mens_boxing_age_per_year[year])
    plt.plot(years,average_age)
    plt.xlabel('Year')
    plt.ylabel('Average age')
    plt.title('Average age for mens in heavy weight boxing')
    plt.show()

def plot_and_calculate_avg_age_heavy_weight_boxing_per_year (athlete):
    avg_mens_boxing_age_per_year=avg_age_heavy_weight_boxing_per_year(athlete)
    plot_avg_age_heavy_weight_boxing_per_year(avg_mens_boxing_age_per_year)

if __name__ == "__main__":
    athlete = extract_csv_data('olympic/athlete_events.csv')
    plot_and_calculate_avg_age_heavy_weight_boxing_per_year(athlete)