import matplotlib.pyplot as plt
import csv
from extract_csv_data import extract_csv_data
import collections

def ratio_m_fem_by_decade(athletes):
    ratio_male_female_per_decade = {}
    for athlete in athletes:
        if int(int(athlete['Year'])/10)*10 in ratio_male_female_per_decade:
            if athlete['Sex'] in ratio_male_female_per_decade[int(int(athlete['Year'])/10)*10]:
                ratio_male_female_per_decade[int(int(athlete['Year'])/10)*10][athlete['Sex']]+=1
            else:
                ratio_male_female_per_decade[int(int(athlete['Year'])/10)*10][athlete['Sex']]= 1
        else:
            ratio_male_female_per_decade[int(int(athlete['Year'])/10)*10] = {athlete['Sex']:1}
    print(ratio_male_female_per_decade)
    return(ratio_male_female_per_decade)


# def plot_ratio_m_fem_by_decade(ratio_male_female_per_decade):
#     m=[]
#     fem=[]
#     year = []
#     for x in ratio['male']:
#         m.append(x)
#     for x in ratio['female']:
#         fem.append(x)
#     for years in range(1890,2010,10):
#         year.append(years)
#     years=[]
#     for y in year:
#         years.append(y+2)
#     plt.bar(year,m,label='Male',color = 'g')
#     plt.bar(years,fem,label = 'Female')
#     plt.legend()
#     plt.xlabel('Decades')
#     plt.ylabel('Ratio')
#     plt.title('Male Female Ratio Per Decade')
#     plt.show()

def plot_and_calculate_ratio_m_fem_by_decade(athletes):
    ratio = ratio_m_fem_by_decade(athletes)
    # plot_ratio_male_female(ratio)

if __name__ == "__main__":
    athletes = extract_csv_data('olympic/athlete_events.csv')
    plot_and_calculate_ratio_m_fem_by_decade(athletes)