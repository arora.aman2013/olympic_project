import matplotlib.pyplot as plt
import random
import operator
import csv
def extracting_data(file):
    reader = csv.DictReader(open(file))
    result={}
    for row in reader:
        for column,value in row.items():
            result.setdefault(column,[]).append(value)

    return result

def countries_with_medal_after_2000(athlete):
    countries_and_their_medals_after_2000 = {}
    for i in range(len(athlete['Year'])):
        if int(athlete['Year'][i]) >= 2000:
            if athlete['Medal'][i] != 'NA':
                if athlete['Medal'][i] in countries_and_their_medals_after_2000:
                    if athlete['NOC'][i] in countries_and_their_medals_after_2000[athlete['Medal'][i]]:
                        countries_and_their_medals_after_2000[athlete['Medal'][i]][athlete['NOC'][i]]+=1
                    else:
                        countries_and_their_medals_after_2000[athlete['Medal'][i]][athlete['NOC'][i]]=1
                else:
                    countries_and_their_medals_after_2000[athlete['Medal'][i]] = {athlete['NOC'][i]:1}
    print(countries_and_their_medals_after_2000)
    return countries_and_their_medals_after_2000

def plot_countries_with_medal_after_2000(countries_and_their_medals_after_2000):
    countries_and_their_medals_after_2000_gold=medal['Gold']
    countries_and_their_medals_after_2000_gold=sorted(countries_and_their_medals_after_2000['Gold'].items(), key=operator.itemgetter(1))
    print(countries_and_their_medals_after_2000_gold)

def plot_and_calculate_countries_with_medal_after_2000(athlete):
    countries_and_their_medals_after_2000 = countries_with_medal_after_2000(athlete)
    # plot(countries_and_their_medals_after_2000)

if __name__ == "__main__":
    athlete = extracting_data('olympic/athlete_events.csv')
    plot_and_calculate_countries_with_medal_after_2000(athlete)