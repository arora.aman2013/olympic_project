import matplotlib.pyplot as plt
import random
import csv
def extracting_data(file):
    reader = csv.DictReader(open(file))
    result={}
    for row in reader:
        for column,value in row.items():
            result.setdefault(column,[]).append(value)

    return result

def no_of_times_each_city_has_hosted_olympics(athletes):
    olympic_hosting_count_by_city = {}
    for i in range(len(athletes['Games'])):
        if athletes['City'][i] in olympic_hosting_count_by_city:
            a=0
            for j in range(len(olympic_hosting_count_by_city[athletes['City'][i]])):
                if athletes['Games'][i] == olympic_hosting_count_by_city[athletes['City'][i]][j]:
                    a = 1
                    break
            if(a!=1):
                olympic_hosting_count_by_city[athletes['City'][i]].append(athletes['Games'][i])
        else:
            olympic_hosting_count_by_city[athletes['City'][i]]=[athletes['Games'][i]]
    for city in olympic_hosting_count_by_city:
        olympic_hosting_count_by_city[city]=len(olympic_hosting_count_by_city[city])
    return olympic_hosting_count_by_city

def plot_no_of_times_each_city_has_hosted_olympics(olympic_hosting_count_by_city):
    no_of_times_city_hosted=[]
    for city in olympic_hosting_count_by_city:
        no_of_times_city_hosted.append(olympic_hosting_count_by_city[city])
    color = []
    for i in range(len(olympic_hosting_count_by_city.keys())):
        color.append('#'+"%06x" % random.randint(0, 0xFFFFFF))
    plt.pie(no_of_times_city_hosted,labels=olympic_hosting_count_by_city,colors=color,autopct='%1.1f%%', shadow=True, startangle=140)
    plt.axis('equal')
    plt.show()

def plot_and_calculate_no_of_times_each_city_has_hosted_olympics(file):
    olympic_hosting_count_by_city=no_of_times_each_city_has_hosted_olympics(file)
    plot_no_of_times_each_city_has_hosted_olympics(olympic_hosting_count_by_city)

if __name__ == "__main__":
    athletes=extracting_data('olympic/athlete_events.csv')
    plot_and_calculate_no_of_times_each_city_has_hosted_olympics(athletes)
