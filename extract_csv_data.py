import csv
def extract_csv_data(file):
    with open(file,newline='') as csvfile:
        reader = csv.DictReader(csvfile)
        athlete = []
        for row in reader:
            athlete.append(row)
        return athlete
    

if __name__ == "__main__":
    extract_csv_data('olympic/athlete_events.csv')